# 2180609316-TranMinhHoang
## Ma so sinh vien: 2180609316
## Ho va ten: Tran Minh Hoang
## Lop: 21DTHD4

USER STORY
|Title:               | Find Student information base on ID Student |
|---------------------| ------------------------------------------------------------ |
|Value Statement:     | As a user, I want to find informations base on student ID so that I can collect the information i need|
|Acceptance Criteria: | Acceptance:|
|                     | Display the overall information base on student ID that similar to the student ID input |
|Definition of Done:  | Unit Test Passed                                             |
|                     | Acceptance Criteria Met                                      |
|                     | Code Reviewed                                                |
|                     | Functional Test Passed                                       |
|                     | Non-Functional Requirements Met                              |
|                     | Product Owner Accepts User Story                             |
|Owner:               | Mr Hoang                                                     |
|UI | ![UI_ScreenShot](https://i.ibb.co/gmjLj2P/Screenshot-2023-10-18-140919.png)|

TEST CASE
|Number Order | Req ID | Test Objective | Test Steps | Expected Result|
|-----|------|----|-----|----|
|1 | REQ-1 | The Searching function is working | 1. Click in the Input ID textbox | Display the student info that have the ID simular to the ID in the textbox |
|   |   |   | 2. Type in the ID student that you want to find |    |
|   |   |   | 3. Click on the Search button |   |
|2 | REQ-2 | View the avatar image | 1. Click in the avata image | Open the Image in the larger size and disable everythings around the image |
|3 | REQ-3 | Close when click in the exit button | 1. Click in the exit button | The Searching Form will close and move the user to home page |

## Họ và tên: Lê Duy
## MSSV: 2180608271
## Lớp: 21DTHD4


| Title               | Admin insert student information                                                                                                                                                                                                                                              |
|---------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Value               | As a admin,  I want to manage student information,  so that i can insert student's information into database                                                                                                                                                                  |
| Acceptance Critetia | Acceptance Criteria 1:   Give that, when I click on the 'insert' button, if successful,   show message and insert information into database|
|						|Acceptance Criteria 2:   Give that, when I click on the 'insert' button, if unsuccessful,   show message, input information again|
| Definition of Done: | Unit Test Passed 
|					|	Acceptance Criteria Met |
|					|	Functional Test Passed |
|					|	Non-Functional Requirements Met Product |
|					|   Owner Accepts User Story                                                                                                                                              |
| Owner:              | LeDuy                                                                                                                                                                                                                                                                         |
| Iteration           | Unscheduled                                                                                                                                                                                                                                                                   |
| Estimate            |                                                                                                                                                                                                                                                                               |
|UI |![Input UI](https://cdn.discordapp.com/attachments/895929156389453854/1160903800622690304/frmInsertUI.png?ex=65365aea&is=6523e5ea&hm=43067de59eb5baebc642573d7fa9c543fe31a6b0b0f7a01b336001c1d5a207b0&)  
=======
| Title: | Student statistical by faculty/year |
| --- | --- |
| Value Statement | Customers can use statistical tables to serve many purposes for teaching and student management |
| Acceptance Criteria | Acceptance 1: |
|  | Extract student information including (student ID, student name, average score, faculty) |
|  | Acceptance 2: |
|  | Make a list containing information (student ID, student name, average score, faculty) of the student |
|  | Acceptance 3: |
|  | Use student information (student ID, student name, average score, faculty) to make a report |
| Definition of Done | Unit Tests Passed |
|  | Acceptance Criteria Met |
|  | Code Reviewed |
|  | Functional Tests Passed |
|  | Non - Functional Requirements Met |
|  | Product Owner Accepts User Story |
| Owner | MR HTL |
|UI |![Input UI](https://gitlab.com/htlxvn/21dthd4-2180608703/-/raw/main/Statistical.imp.png?ref_type=heads)
